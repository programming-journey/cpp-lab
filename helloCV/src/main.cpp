#include <string>
#include "opencv2/opencv.hpp"
#include "fmt/format.h"
using namespace std;
using namespace fmt;
using namespace cv;

auto enlarge_image = [](auto& image, auto size){
	Mat large;
	resize(image, large, Size(), size, size, INTER_AREA);
	imwrite("large.jpeg", large);
	return large;
};

auto gamma_corrections = [](auto& image, auto gamma){
	//更改Lookup table（轉換顏色的方法）
	Mat lookUpTable(1, 256, CV_8U);
	auto p = lookUpTable.ptr();
	for(auto i = 0;i<256;i++){
    	p[i] = saturate_cast<uchar>(pow(i / 255.0, gamma) * 255.0);
    }
    //應用方法
    auto gamma_image = image.clone();
    LUT(image, lookUpTable, gamma_image);
    return gamma_image;
};

auto compares = [](auto& old, auto& newer){
	Mat compare;
	hconcat(old, newer, compare);
	return compare;
};

Mat equalizeIntensity(const Mat& inputImage)
{
    if(inputImage.channels() >= 3)
    {
        Mat ycrcb;
        cvtColor(inputImage,ycrcb,COLOR_BGR2YCrCb);

        vector<Mat> channels;
        split(ycrcb,channels);

        equalizeHist(channels[0], channels[0]);

        Mat result;
        merge(channels,ycrcb);
        cvtColor(ycrcb,result,COLOR_YCrCb2BGR);

        return result;
    }

    return Mat();
}

int main(int argc, char **argv)
{
    Mat image; auto path = ""s;
    print("What is the path of image: ");
    cin >> path;
    image = imread(path, 1);
 	auto result = gamma_corrections(image, 0.4); 
 	result = equalizeIntensity(image);
 	auto compare_image = compares(image, result);
 	result = enlarge_image(result,1.15);
 	imwrite("gamma.jpeg",result);

    namedWindow("Display Image", WINDOW_AUTOSIZE);
    imshow("Display Image", compare_image);
    waitKey(0);
    
    return 0;
}