#include <vector>
#include <string>
#include "fmt/format.h" //這東西如果你不喜歡可以換成iostream
#include "nlohmann/json.hpp"
#include<map>
using json = nlohmann::json; //你可以改其他名字，例如JSON這樣
using namespace fmt;
using namespace std;
auto Dog = [](){
    print("HI");
};
auto bark = [](){
    print("WOO");
};



int main(){
    json person;
    person["age"] = 32;
    person["name"] = "Amanda";
    person["gender"] = "F";
    person["contact"]["address"] = "Taiwan";
    person["contact"]["phone"] = "24331276";
    person["weight"] = 47.6;
    person["happy"] = true;
    vector<int> v2 = {1,2,3,4};
    std::vector<std::uint8_t> v = {'t', 'r', 'u', 'e'};
    person["nebye"] = json::parse(v);
    json jsonv(v2);
    person["vecs"] = jsonv;
    auto persons = person;
    persons["amd"] = "Ryzen";
    //persons["amd"] = Dog;
    
    print("{}\n",person.dump(4));
    map<string, decltype(bark)> hw;
    hw.emplace("Hi",bark);

}
/*
int main(int argc, char **argv)
{

    using namespace fmt;
    auto Hi = "123"s;
    auto Hi2 = "456"s;
    print("{} {}\n", Hi, Hi2);
    auto a = 4;
    auto b = 4;
    auto operand = '+';
    auto equation = format("{0} {1} {2}\n", a, operand, b);
    print(equation);
    
    print("Hi");

    cv::Mat image;
    image = cv::imread(argv[1], 1);
    cv::Mat large;
    cv::resize(image, large, cv::Size(), 10.0, 10.0, cv::INTER_AREA);
    imwrite("large.jpeg", large);

    namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    imshow("Display Image", large);
    cv::waitKey(0);
    
    return 0;
}
*/